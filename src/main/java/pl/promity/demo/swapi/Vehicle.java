package pl.promity.demo.swapi;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Vehicle {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("manufacturer")
    @Expose
    private String manufacturer;
    @SerializedName("cost_in_credits")
    @Expose
    private String costInCredits;
    @SerializedName("length")
    @Expose
    private String length;
    @SerializedName("max_atmosphering_speed")
    @Expose
    private String maxAtmospheringSpeed;
    @SerializedName("crew")
    @Expose
    private String crew;
    @SerializedName("passengers")
    @Expose
    private String passengers;
    @SerializedName("cargo_capacity")
    @Expose
    private String cargoCapacity;
    @SerializedName("consumables")
    @Expose
    private String consumables;
    @SerializedName("vehicle_class")
    @Expose
    private String vehicleClass;
    @SerializedName("pilots")
    @Expose
    private List<Object> pilots = new ArrayList<Object>();
    @SerializedName("films")
    @Expose
    private List<Integer> films = new ArrayList<Integer>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Vehicle() {
    }

    /**
     *
     * @param id
     * @param name
     * @param model
     * @param manufacturer
     * @param costInCredits
     * @param length
     * @param maxAtmospheringSpeed
     * @param crew
     * @param passengers
     * @param cargoCapacity
     * @param consumables
     * @param vehicleClass
     * @param pilots
     * @param films
     */
    public Vehicle(long id, String name, String model, String manufacturer, String costInCredits, String length, String maxAtmospheringSpeed, String crew, String passengers, String cargoCapacity, String consumables, String vehicleClass, List<Object> pilots, List<Integer> films) {
        super();
        this.id = id;
        this.name = name;
        this.model = model;
        this.manufacturer = manufacturer;
        this.costInCredits = costInCredits;
        this.length = length;
        this.maxAtmospheringSpeed = maxAtmospheringSpeed;
        this.crew = crew;
        this.passengers = passengers;
        this.cargoCapacity = cargoCapacity;
        this.consumables = consumables;
        this.vehicleClass = vehicleClass;
        this.pilots = pilots;
        this.films = films;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getCostInCredits() {
        return costInCredits;
    }

    public void setCostInCredits(String costInCredits) {
        this.costInCredits = costInCredits;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getMaxAtmospheringSpeed() {
        return maxAtmospheringSpeed;
    }

    public void setMaxAtmospheringSpeed(String maxAtmospheringSpeed) {
        this.maxAtmospheringSpeed = maxAtmospheringSpeed;
    }

    public String getCrew() {
        return crew;
    }

    public void setCrew(String crew) {
        this.crew = crew;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public String getCargoCapacity() {
        return cargoCapacity;
    }

    public void setCargoCapacity(String cargoCapacity) {
        this.cargoCapacity = cargoCapacity;
    }

    public String getConsumables() {
        return consumables;
    }

    public void setConsumables(String consumables) {
        this.consumables = consumables;
    }

    public String getVehicleClass() {
        return vehicleClass;
    }

    public void setVehicleClass(String vehicleClass) {
        this.vehicleClass = vehicleClass;
    }

    public List<Object> getPilots() {
        return pilots;
    }

    public void setPilots(List<Object> pilots) {
        this.pilots = pilots;
    }

    public List<Integer> getFilms() {
        return films;
    }

    public void setFilms(List<Integer> films) {
        this.films = films;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("model", model)
                .append("manufacturer", manufacturer)
                .append("costInCredits", costInCredits)
                .append("length", length)
                .append("maxAtmospheringSpeed", maxAtmospheringSpeed)
                .append("crew", crew)
                .append("passengers", passengers)
                .append("cargoCapacity", cargoCapacity)
                .append("consumables", consumables)
                .append("vehicleClass", vehicleClass)
                .append("pilots", pilots)
                .append("films", films)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Vehicle vehicle = (Vehicle) o;

        return new EqualsBuilder()
                .append(getId(), vehicle.getId())
                .append(getName(), vehicle.getName())
                .append(getModel(), vehicle.getModel())
                .append(getManufacturer(), vehicle.getManufacturer())
                .append(getCostInCredits(), vehicle.getCostInCredits())
                .append(getLength(), vehicle.getLength())
                .append(getMaxAtmospheringSpeed(), vehicle.getMaxAtmospheringSpeed())
                .append(getCrew(), vehicle.getCrew())
                .append(getPassengers(), vehicle.getPassengers())
                .append(getCargoCapacity(), vehicle.getCargoCapacity())
                .append(getConsumables(), vehicle.getConsumables())
                .append(getVehicleClass(), vehicle.getVehicleClass())
                .append(getPilots(), vehicle.getPilots())
                .append(getFilms(), vehicle.getFilms())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(getName())
                .append(getModel())
                .append(getManufacturer())
                .append(getCostInCredits())
                .append(getLength())
                .append(getMaxAtmospheringSpeed())
                .append(getCrew())
                .append(getPassengers())
                .append(getCargoCapacity())
                .append(getConsumables())
                .append(getVehicleClass())
                .append(getPilots())
                .append(getFilms())
                .toHashCode();
    }

    public long getId() {
        return id;
    }

}