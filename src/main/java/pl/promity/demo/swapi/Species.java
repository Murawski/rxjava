package pl.promity.demo.swapi;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Species {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("classification")
    @Expose
    private String classification;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("average_height")
    @Expose
    private String averageHeight;
    @SerializedName("skin_colors")
    @Expose
    private String skinColors;
    @SerializedName("hair_colors")
    @Expose
    private String hairColors;
    @SerializedName("eye_colors")
    @Expose
    private String eyeColors;
    @SerializedName("average_lifespan")
    @Expose
    private String averageLifespan;
    @SerializedName("homeworld")
    @Expose
    private Object homeworld;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("people")
    @Expose
    private List<Integer> people = new ArrayList<Integer>();
    @SerializedName("films")
    @Expose
    private List<Integer> films = new ArrayList<Integer>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Species() {
    }

    /**
     *
     * @param id
     * @param name
     * @param classification
     * @param designation
     * @param averageHeight
     * @param skinColors
     * @param hairColors
     * @param eyeColors
     * @param averageLifespan
     * @param homeworld
     * @param language
     * @param people
     * @param films
     */
    public Species(long id, String name, String classification, String designation, String averageHeight, String skinColors, String hairColors, String eyeColors, String averageLifespan, Object homeworld, String language, List<Integer> people, List<Integer> films) {
        super();
        this.id = id;
        this.name = name;
        this.classification = classification;
        this.designation = designation;
        this.averageHeight = averageHeight;
        this.skinColors = skinColors;
        this.hairColors = hairColors;
        this.eyeColors = eyeColors;
        this.averageLifespan = averageLifespan;
        this.homeworld = homeworld;
        this.language = language;
        this.people = people;
        this.films = films;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getAverageHeight() {
        return averageHeight;
    }

    public void setAverageHeight(String averageHeight) {
        this.averageHeight = averageHeight;
    }

    public String getSkinColors() {
        return skinColors;
    }

    public void setSkinColors(String skinColors) {
        this.skinColors = skinColors;
    }

    public String getHairColors() {
        return hairColors;
    }

    public void setHairColors(String hairColors) {
        this.hairColors = hairColors;
    }

    public String getEyeColors() {
        return eyeColors;
    }

    public void setEyeColors(String eyeColors) {
        this.eyeColors = eyeColors;
    }

    public String getAverageLifespan() {
        return averageLifespan;
    }

    public void setAverageLifespan(String averageLifespan) {
        this.averageLifespan = averageLifespan;
    }

    public Object getHomeworld() {
        return homeworld;
    }

    public void setHomeworld(Object homeworld) {
        this.homeworld = homeworld;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<Integer> getPeople() {
        return people;
    }

    public void setPeople(List<Integer> people) {
        this.people = people;
    }

    public List<Integer> getFilms() {
        return films;
    }

    public void setFilms(List<Integer> films) {
        this.films = films;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("classification", classification)
                .append("designation", designation)
                .append("averageHeight", averageHeight)
                .append("skinColors", skinColors)
                .append("hairColors", hairColors)
                .append("eyeColors", eyeColors)
                .append("averageLifespan", averageLifespan)
                .append("homeworld", homeworld)
                .append("language", language)
                .append("people", people)
                .append("films", films)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Species species = (Species) o;

        return new EqualsBuilder()
                .append(getId(), species.getId())
                .append(getName(), species.getName())
                .append(getClassification(), species.getClassification())
                .append(getDesignation(), species.getDesignation())
                .append(getAverageHeight(), species.getAverageHeight())
                .append(getSkinColors(), species.getSkinColors())
                .append(getHairColors(), species.getHairColors())
                .append(getEyeColors(), species.getEyeColors())
                .append(getAverageLifespan(), species.getAverageLifespan())
                .append(getHomeworld(), species.getHomeworld())
                .append(getLanguage(), species.getLanguage())
                .append(getPeople(), species.getPeople())
                .append(getFilms(), species.getFilms())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(getName())
                .append(getClassification())
                .append(getDesignation())
                .append(getAverageHeight())
                .append(getSkinColors())
                .append(getHairColors())
                .append(getEyeColors())
                .append(getAverageLifespan())
                .append(getHomeworld())
                .append(getLanguage())
                .append(getPeople())
                .append(getFilms())
                .toHashCode();
    }

    public long getId() {
        return id;
    }

}