package pl.promity.demo.swapi;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Film {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("episode_id")
    @Expose
    private Integer episodeId;
    @SerializedName("opening_crawl")
    @Expose
    private String openingCrawl;
    @SerializedName("director")
    @Expose
    private String director;
    @SerializedName("producer")
    @Expose
    private String producer;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;
    @SerializedName("characters")
    @Expose
    private List<Integer> characters = new ArrayList<Integer>();
    @SerializedName("planets")
    @Expose
    private List<Integer> planets = new ArrayList<Integer>();
    @SerializedName("starships")
    @Expose
    private List<Integer> starships = new ArrayList<Integer>();
    @SerializedName("vehicles")
    @Expose
    private List<Integer> vehicles = new ArrayList<Integer>();
    @SerializedName("species")
    @Expose
    private List<Integer> species = new ArrayList<Integer>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Film() {
    }

    public Film(long id, String title, Integer episodeId, String openingCrawl, String director, String producer, String releaseDate, List<Integer> characters, List<Integer> planets, List<Integer> starships, List<Integer> vehicles, List<Integer> species) {
        super();
        this.id = id;
        this.title = title;
        this.episodeId = episodeId;
        this.openingCrawl = openingCrawl;
        this.director = director;
        this.producer = producer;
        this.releaseDate = releaseDate;
        this.characters = characters;
        this.planets = planets;
        this.starships = starships;
        this.vehicles = vehicles;
        this.species = species;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getEpisodeId() {
        return episodeId;
    }

    public void setEpisodeId(Integer episodeId) {
        this.episodeId = episodeId;
    }

    public String getOpeningCrawl() {
        return openingCrawl;
    }

    public void setOpeningCrawl(String openingCrawl) {
        this.openingCrawl = openingCrawl;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Integer> getCharacters() {
        return characters;
    }

    public void setCharacters(List<Integer> characters) {
        this.characters = characters;
    }

    public List<Integer> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Integer> planets) {
        this.planets = planets;
    }

    public List<Integer> getStarships() {
        return starships;
    }

    public void setStarships(List<Integer> starships) {
        this.starships = starships;
    }

    public List<Integer> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Integer> vehicles) {
        this.vehicles = vehicles;
    }

    public List<Integer> getSpecies() {
        return species;
    }

    public void setSpecies(List<Integer> species) {
        this.species = species;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("title", title)
                .append("episodeId", episodeId)
                .append("openingCrawl", openingCrawl)
                .append("director", director)
                .append("producer", producer)
                .append("releaseDate", releaseDate)
                .append("characters", characters)
                .append("planets", planets)
                .append("starships", starships)
                .append("vehicles", vehicles)
                .append("species", species)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Film film = (Film) o;

        return new EqualsBuilder()
                .append(getId(), film.getId())
                .append(getTitle(), film.getTitle())
                .append(getEpisodeId(), film.getEpisodeId())
                .append(getOpeningCrawl(), film.getOpeningCrawl())
                .append(getDirector(), film.getDirector())
                .append(getProducer(), film.getProducer())
                .append(getReleaseDate(), film.getReleaseDate())
                .append(getCharacters(), film.getCharacters())
                .append(getPlanets(), film.getPlanets())
                .append(getStarships(), film.getStarships())
                .append(getVehicles(), film.getVehicles())
                .append(getSpecies(), film.getSpecies())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(getTitle())
                .append(getEpisodeId())
                .append(getOpeningCrawl())
                .append(getDirector())
                .append(getProducer())
                .append(getReleaseDate())
                .append(getCharacters())
                .append(getPlanets())
                .append(getStarships())
                .append(getVehicles())
                .append(getSpecies())
                .toHashCode();
    }

    public long getId() {
        return id;
    }

}