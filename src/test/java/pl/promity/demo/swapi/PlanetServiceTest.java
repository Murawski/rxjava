package pl.promity.demo.swapi;

import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import pl.promity.demo.swapi.Planet;
import pl.promity.demo.swapi.PlanetService;

import static org.junit.Assert.*;

public class PlanetServiceTest {

    private PlanetService objectUnderTest;

    @Before
    public void setUp() throws Exception {
        this.objectUnderTest = new PlanetService();
    }

    @Test
    public void shouldCreateOrderServiceCorrectly() {
        assertNotNull(this.objectUnderTest);
    }

    @Test
    public void shouldReturnAllPlanets() throws Exception {
        //given
        TestObserver<Planet> testSubscriber = new TestObserver<>();

        //when
        objectUnderTest.findAll().subscribe(testSubscriber);

        //then
        testSubscriber.assertValueCount(58);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();

    }

}