package pl.promity.demo;

import com.google.common.collect.Lists;
import io.reactivex.*;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.functions.Functions;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class IntroductionTest {

    private static final ExecutorService THREAD_POOL = Executors.newFixedThreadPool(5);
    private static final Scheduler SCHEDULER_THREAD_POOL = Schedulers.from(THREAD_POOL);

    // ----------------------------------------------------------------------------------------------- #intro

    @Test
    public void blocking() throws Exception {
        Logger.log("START TEST");
        Set<String> genres = MusicService.blocking_getGenres();
        Logger.log(genres);
    }

    @Test
    public void future() throws Exception {
        Logger.log("START TEST");
        Future<Set<String>> future = THREAD_POOL.submit(MusicService::blocking_getGenres);
        Logger.log("SUBMIT");
        Set<String> genres = future.get();
        Logger.log(genres);
    }

    @Test
    public void java8_completableFuture() throws Exception {
        Logger.log("START TEST");
        CompletableFuture<Set<String>> completableFuture = CompletableFuture.supplyAsync(MusicService::blocking_getGenres, THREAD_POOL);
        Logger.log("SUBMIT");
        completableFuture.thenApply(set -> set.stream().map(String::toLowerCase).collect(Collectors.toList()))
                .whenCompleteAsync((i, t) -> {
                    Logger.log("RESULT %s", i);
                    Logger.log("EXCEPTION %s", t);
                });
        Logger.log("TEST FINISHED");
    }

    @Test
    public void rxJava() throws Exception {
        Logger.log("START TEST");
        Observable<Set<String>> observable = Observable.fromCallable(MusicService::blocking_getGenres);
        Logger.log("SUBMIT");
        observable
                .map(set -> set.stream().map(String::toLowerCase).collect(Collectors.toList()))
                .subscribe(
                        Logger::log,
                        Logger::log
                );
        Logger.log("TEST FINISHED");
    }

    @Test
    public void observable_blocking() {
        Observable.fromIterable(Lists.newArrayList("A", "B", "C", "D"))
                .map(String::toLowerCase)
                .blockingIterable()
                .forEach(Logger::log);
    }

    @Test
    public void observable_callbacks() {
        Observable.fromIterable(Lists.newArrayList("A", "B", "C", "D"))
                .map(String::toLowerCase)
                .subscribe(
                        s -> Logger.log("Next element %s", s),
                        e -> Logger.log("Error! " + e),
                        () -> Logger.log("Completed")
                );
    }

    @Test
    public void observable_callbacks_error() {
        Observable.fromIterable(Lists.newArrayList("A", "B", "C", "D"))
                .concatWith(Observable.error(new IllegalStateException()))
                .map(String::toLowerCase)
                .subscribe(
                        s -> Logger.log("Next element %s", s),
                        e -> Logger.log("Error! " + e),
                        () -> Logger.log("Completed")
                );
    }

    // ----------------------------------------------------------------------------------------------- #types


    @Test
    public void observer() {
        Observable.fromIterable(Lists.newArrayList("A", "B", "C", "D"))
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Logger.log("Subscribed!");
                    }

                    @Override
                    public void onNext(String s) {
                        Logger.log("Next element %s", s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.log("Error! " + e);
                    }

                    @Override
                    public void onComplete() {
                        Logger.log("Completed");
                    }
                });
    }

    @Test
    public void completable() {
        Completable.fromCallable(() -> Lists.newArrayList("A", "B", "C", "D"))
                .subscribe(
                        () -> Logger.log("Completed"),
                        e -> Logger.log("Error! " + e)
                );
    }

    @Test
    public void single() {
        // ERROR
        Observable.empty()
                .firstOrError()
                .subscribe(
                        single -> Logger.log(single),
                        error -> Logger.log(error)
                );
        // OK
        Observable.just(1)
                .firstOrError()
                .subscribe(
                        single -> Logger.log(single),
                        error -> Logger.log(error)
                );
    }

    @Test
    public void maybe() {
        Observable.empty()
                .firstElement()
                .subscribe(
                        single -> Logger.log(single),
                        error -> Logger.log(error),
                        () -> Logger.log("FINISHED!")
                );
    }

    @Test
    public void flowable() {
        Flowable.fromIterable(Lists.newArrayList(1, 2, 3, 4, 5, 6))
                .map(i -> i * i)
                .subscribe(
                        single -> Logger.log(single),
                        error -> Logger.log(error),
                        () -> Logger.log("FINISHED!")
                );
    }

    @Test
    public void publish_subject() throws Exception {
        PublishSubject<Integer> publishSubject = PublishSubject.create();

        publishSubject.subscribe(Logger::log, Logger::log, () -> Logger.log("FINISHED"));
        Logger.log("SUBSCRIBED!");

        publishSubject.onNext(1);
        publishSubject.onNext(2);
        publishSubject.onNext(3);

        publishSubject.subscribe(Logger::log, Logger::log, () -> Logger.log("FINISHED"));
        Logger.log("SUBSCRIBED!");
        publishSubject.onNext(4);

        publishSubject.onComplete();

    }

    @Test
    public void replay_subject() throws Exception {
        ReplaySubject<Integer> replaySubject = ReplaySubject.create(50);

        replaySubject.subscribe(Logger::log, Logger::log, () -> Logger.log("FINISHED"));
        Logger.log("SUBSCRIBED!");

        replaySubject.onNext(1);
        replaySubject.onNext(2);
        replaySubject.onNext(3);

        replaySubject.subscribe(Logger::log, Logger::log, () -> Logger.log("FINISHED"));
        Logger.log("SUBSCRIBED!");
        replaySubject.onNext(4);

        replaySubject.onComplete();

    }

    // ----------------------------------------------------------------------------------------------- #create

    // single value observable
    @Test
    public void create_just() {
        Observable.just("ABCD")
                .subscribe(
                        next -> Logger.log(next),
                        error -> Logger.log(error),
                        () -> Logger.log("FINISHED!")
                );
    }

    // single value observable
    @Test
    public void create_fromCallable() {
        Observable.fromCallable(Math::random)
                .subscribe(Logger::log);
    }

    @Test
    public void create_fromIterable() {
        Observable.fromCallable(() -> Lists.newArrayList("A", "B", "C", "D"))
                .subscribe();
    }

    @Test
    public void create_error() {
        Observable.error(new IllegalStateException("yolo!"))
                .subscribe(
                        next -> Logger.log(next),
                        error -> Logger.log(error),
                        () -> Logger.log("FINISHED!")
                );
    }

    // oh no
    @Test
    public void create_interval() {
        Observable.interval(1, TimeUnit.SECONDS)
                .take(5)
                .subscribe(Logger::log);
    }


    // ----------------------------------------------------------------------------------------------- #operators

    @Test
    public void filter() {
        List<Character> letters = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.fromIterable(letters)
                .filter(Character::isUpperCase)
                .subscribe(Logger::log);

    }

    @Test
    public void flatMap() {
        MusicService.rx_getGenres()
                .flatMap(MusicService::rx_getBandsByGenre)
                .subscribe(Logger::log);
    }

    @Test
    public void flatMapIterable() {

        // get single band for each genre
        Observable.fromIterable(MusicService.getGenreToBandMapping().values())
                .flatMap(Observable::fromIterable)
                .subscribe(Logger::log);

        // get single band for each genre
        Observable.fromIterable(MusicService.getGenreToBandMapping().values())
                .flatMapIterable(Functions.identity())
                .subscribe(Logger::log);

    }

    @Test
    public void zipWith() {
        List<Character> letters = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.fromIterable(letters)
                .zipWith(Observable.just(1).repeat().scan(0, (acc, in) -> acc + in), (letter, index) -> letter + ":" + index)
                .subscribe(Logger::log);

    }

    @Test
    public void take_skip() {
        List<Character> letters = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'g', 'h', 'i', 'j', 'k', 'l');

        Logger.log("TAKE");
        Observable.fromIterable(letters)
                .take(8)
                .subscribe(Logger::log);

        Logger.log("SKIP");
        Observable.fromIterable(letters)
                .skip(4)
                .subscribe(Logger::log);

    }

    @Test
    public void takeUntil() {
        List<Character> letters = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.fromIterable(letters)
                .takeUntil((Predicate<Character>) Character::isLowerCase)
                .subscribe(Logger::log);

    }

    @Test
    public void takeWhile() {
        List<Character> letters = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.fromIterable(letters)
                .takeWhile(Character::isUpperCase)
                .subscribe(Logger::log);

    }

    @Test
    public void takeUntil_observable() {
        List<Character> letters = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.fromIterable(letters).repeat()
                .takeUntil(Observable.interval(1, TimeUnit.SECONDS))
                .subscribe(Logger::log);

    }

    // memory problem
    @Test
    public void distinct() {
        List<Character> letters = Lists.newArrayList('A', 'A', 'C', 'D', 'A', 'B', 'g', 'i', 'i', 'j', 'k', 'l');

        Observable.fromIterable(letters)
                .distinct()
                .subscribe(Logger::log);

    }

    @Test
    public void groupBy() {
        List<Character> letters = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.fromIterable(letters)
                .groupBy(Character::isLowerCase)
                .map(x -> x.collectInto(Lists.newArrayList(), ArrayList::add))
                .flatMap(Single::toObservable)
                .subscribe(Logger::log);

    }

    @Test
    public void merge() throws InterruptedException {
        List<Character> lettersUpper = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L');
        List<Character> lettersLower = Lists.newArrayList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.merge(
                Observable.fromIterable(lettersUpper).zipWith(Observable.interval(100, TimeUnit.MILLISECONDS), (l, i) -> l).repeat(),
                Observable.fromIterable(lettersLower).zipWith(Observable.interval(90, TimeUnit.MILLISECONDS), (l, i) -> l).repeat()
        )
                .takeUntil(Observable.interval(2, TimeUnit.SECONDS))
                .subscribe(Logger::log);

        Thread.sleep(5000);
    }

    @Test
    public void concat() throws InterruptedException {
        List<Character> lettersUpper = Lists.newArrayList('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L');
        List<Character> lettersLower = Lists.newArrayList('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l');

        Observable.concat(
                Observable.fromIterable(lettersUpper).zipWith(Observable.interval(100, TimeUnit.MILLISECONDS), (l, i) -> l),
                Observable.fromIterable(lettersLower).zipWith(Observable.interval(90, TimeUnit.MILLISECONDS), (l, i) -> l)
        )
                .subscribe(Logger::log);

        Thread.sleep(5000);
    }

    @Test
    public void observeOn() throws Exception {

        Observable.fromCallable( () -> MusicService.blocking_getBandsByGenre("DISCO"))
                .observeOn(Schedulers.computation())
                .flatMapIterable(Functions.identity())
                .map(String::toUpperCase)
                .doOnNext(next -> Logger.log(next))
                .observeOn(SCHEDULER_THREAD_POOL)
                .subscribe(
                        next -> Logger.log(next),
                        error -> Logger.log(error),
                        () -> Logger.log("FINISHED!")
                );

        Thread.sleep(2000);

    }

    @Test
    public void subscribeOn() throws Exception {

        Observable.fromCallable( () -> MusicService.blocking_getBandsByGenre("DISCO"))
                .subscribeOn(Schedulers.computation())
                .flatMapIterable(Functions.identity())
                .map(String::toUpperCase)
                .doOnNext(next -> Logger.log(next))
                .subscribeOn(SCHEDULER_THREAD_POOL)
                .subscribe(
                        next -> Logger.log(next),
                        error -> Logger.log(error),
                        () -> Logger.log("FINISHED!")
                );

        Thread.sleep(2000);

    }

    @Test
    public void monitoring() throws Exception {
        MusicService.rx_getGenres_delayed()
                .doOnNext(i -> Logger.log("NEXT: %s", i))
                .doOnError(i -> Logger.log("ERROR: %s", i))
                .doOnComplete(() -> Logger.log("COMPLETE"))
                .blockingLast();
    }

    @Test
    public void eror_handling() throws Exception {
        String last = MusicService.rx_getGenres_delayed()
                .timeout(1, TimeUnit.SECONDS)
                .doOnNext(i -> Logger.log("NEXT: %s", i))
                .doOnError(i -> Logger.log("ERROR: %s", i))
                .onErrorReturnItem("ERROR")
                .blockingLast();
        Logger.log(last);
    }
}