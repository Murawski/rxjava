package pl.promity.demo;

import com.google.common.collect.Lists;
import io.reactivex.observers.TestObserver;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Before;
import org.junit.Test;
import pl.promity.demo.swapi.*;

public class WorkshopTest {

    private Workshop objectUnderTest;

    @Before
    public void setUp() throws Exception {
        objectUnderTest = new Workshop(
                new FilmService(),
                new PersonService(),
                new PlanetService(),
                new SpeciesService(),
                new StarshipService(),
                new VehicleService()
        );
    }

    @Test
    public void findTop3CommonFilmCharactersWithAppearanceCount() throws Exception {
        //given
        TestObserver<Pair<String, Integer>> testSubscriber = new TestObserver<>();

        //when
        objectUnderTest.findTop3CommonFilmCharactersWithAppearanceCount()
                .map(p -> Pair.of(p.getLeft().getName(), p.getRight()))
                .subscribe(testSubscriber);

        //then
        testSubscriber.assertValueCount(3)
                .assertValueSequence(
                    Lists.newArrayList(
                        Pair.of("R2-D2", 7),
                        Pair.of("Obi-Wan Kenobi", 6),
                        Pair.of("C-3PO", 6)
                    )
                )
                .assertNoErrors()
                .assertComplete();
    }

}