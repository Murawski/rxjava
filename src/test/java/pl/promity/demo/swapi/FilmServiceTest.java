package pl.promity.demo.swapi;

import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import pl.promity.demo.swapi.Film;
import pl.promity.demo.swapi.FilmService;

import static org.junit.Assert.*;

public class FilmServiceTest {

    private FilmService objectUnderTest;

    @Before
    public void setUp() throws Exception {
        this.objectUnderTest = new FilmService();
    }

    @Test
    public void shouldCreateOrderServiceCorrectly() {
        assertNotNull(this.objectUnderTest);
    }

    @Test
    public void shouldReturnAllFilms() throws Exception {
        //given
        TestObserver<Film> testSubscriber = new TestObserver<>();

        //when
        objectUnderTest.findAll().subscribe(testSubscriber);

        //then
        testSubscriber.assertValueCount(7);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();

    }


}