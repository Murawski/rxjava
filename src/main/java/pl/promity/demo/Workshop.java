package pl.promity.demo;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Ordering;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.internal.functions.Functions;
import org.apache.commons.lang3.tuple.Pair;
import pl.promity.demo.swapi.*;

import java.util.*;

public class Workshop {

    private final FilmService filmService;
    private final PersonService personService;
    private final PlanetService planetService;
    private final SpeciesService speciesService;
    private final StarshipService starshipService;
    private final VehicleService vehicleService;


    public Workshop(FilmService filmService, PersonService personService, PlanetService planetService,
                    SpeciesService speciesService, StarshipService starshipService, VehicleService vehicleService) {
        this.filmService = filmService;
        this.personService = personService;
        this.planetService = planetService;
        this.speciesService = speciesService;
        this.starshipService = starshipService;
        this.vehicleService = vehicleService;
    }

    // take, skip
    public final Observable<Person> findAllPaging(int pageSize, int pageNumber){
        return personService.findAll().skip(pageNumber*pageSize).take(pageSize);
//        return Observable.empty();
    }

    // filter, flatMap, distinct
    public final Single<Long> countAllStarshipsInFilmsDirectedByGeorgeLucas(){
        return filmService.findAll()
                .filter(f -> f.getDirector().equals("George Lucas"))
                .flatMapIterable(Film::getStarships)
                .flatMap(id -> starshipService.findById(id).toObservable())
                .distinct()
                .count();
//        return Observable.<Integer>empty().singleOrError();
    }

    // groupBy
    public final Observable<Pair<Person, Integer>> findTop3CommonFilmCharactersWithAppearanceCount(){
        return filmService.findAll()
                .flatMapIterable(Film::getCharacters)
                .flatMap(c -> personService.findById(c).toObservable())
                .groupBy(Functions.identity(), Functions.identity())
                .flatMap(
                    g -> g.collectInto(MultimapBuilder.<Person, Long>hashKeys().arrayListValues().build(),
                        (map, p) -> map.put(p, p.getId())).toObservable()
                )
                .map(ListMultimap::asMap)
                .flatMapIterable(Map::entrySet)
                .map(e -> Pair.of((Person) e.getKey(), e.getValue().size() ))
                .sorted(Comparator.comparing(Pair::getRight, (o1, o2) -> Ordering.natural().reverse().compare(o1, o2)))
                .take(3);
//        return Observable.<Pair<Person, Integer>>empty().singleOrError();
    }

    // concat
    public final Maybe<Species> findCachedSpeciesByName(String name){
        return Observable.<Species>empty().singleElement();
    }



}
