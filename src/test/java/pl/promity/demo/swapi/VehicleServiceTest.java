package pl.promity.demo.swapi;

import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import pl.promity.demo.swapi.Vehicle;
import pl.promity.demo.swapi.VehicleService;

import static org.junit.Assert.*;

public class VehicleServiceTest {

    private VehicleService objectUnderTest;

    @Before
    public void setUp() throws Exception {
        this.objectUnderTest = new VehicleService();
    }

    @Test
    public void shouldCreateOrderServiceCorrectly() {
        assertNotNull(this.objectUnderTest);
    }

    @Test
    public void shouldReturnAllVehicles() throws Exception {
        //given
        TestObserver<Vehicle> testSubscriber = new TestObserver<>();

        //when
        objectUnderTest.findAll().subscribe(testSubscriber);

        //then
        testSubscriber.assertValueCount(39);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();

    }


}