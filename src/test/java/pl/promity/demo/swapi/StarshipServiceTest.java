package pl.promity.demo.swapi;

import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import pl.promity.demo.swapi.Starship;
import pl.promity.demo.swapi.StarshipService;

import static org.junit.Assert.*;

public class StarshipServiceTest {

    private StarshipService objectUnderTest;

    @Before
    public void setUp() throws Exception {
        this.objectUnderTest = new StarshipService();
    }

    @Test
    public void shouldCreateOrderServiceCorrectly() {
        assertNotNull(this.objectUnderTest);
    }

    @Test
    public void shouldReturnAllStarships() throws Exception {
        //given
        TestObserver<Starship> testSubscriber = new TestObserver<>();

        //when
        objectUnderTest.findAll().subscribe(testSubscriber);

        //then
        testSubscriber.assertValueCount(37);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();

    }

}