package pl.promity.demo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatterBuilder;

import static java.time.temporal.ChronoField.*;

// Just for this project
public interface Logger {

    static void log(Object message, Object... params){
        System.out.println(
                        "["+String.format("%15s", Thread.currentThread().getName())+"]" +
                        "["+ LocalDateTime.now().format(new DateTimeFormatterBuilder()
                        .appendValue(HOUR_OF_DAY, 2)
                        .appendLiteral(':')
                        .appendValue(MINUTE_OF_HOUR, 2)
                        .optionalStart()
                        .appendLiteral(':')
                        .appendValue(SECOND_OF_MINUTE, 2)
                        .appendLiteral(':')
                        .appendValue(MILLI_OF_SECOND, 3).toFormatter())+"] " +
                        String.format(String.valueOf(message), params));
    }

}
