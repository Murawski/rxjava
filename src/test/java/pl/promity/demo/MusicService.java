package pl.promity.demo;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import io.reactivex.Observable;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

class MusicService {

    private static final Map<String, List<String>> genreToBandMapping = ImmutableMap.of(
            "METAL", Lists.newArrayList("The Metal Street Artists's Club", "Rock Rock", "Metal Division", "King Yellow"),
            "DISCO", Lists.newArrayList("One Hundred Inch Bananas", "Of Men and Elephants", "The Working Bananas", "The Disco Heroes of Cardiff"),
            "RAP", Lists.newArrayList("The Gangsta Can For the Goblins", "Rap Fishing Rod", "Fishing Rod Fighters", "McFishing Rod")
    );

    static Observable<String> rx_getGenres(){
        return Observable.fromIterable(genreToBandMapping.keySet());
    }

    static Observable<String> rx_getBandsByGenre(String genre){
        return Observable.fromIterable(genreToBandMapping.get(genre));
    }

    static Observable<String> rx_getGenres_delayed(){
        return Observable.fromIterable(genreToBandMapping.keySet())
                .zipWith(Observable.interval((long) (Math.random()*1000 + 500), TimeUnit.MILLISECONDS), (g, i) -> g);
    }

    static Set<String> blocking_getGenres(){
        sleepQuietly(500);
        return genreToBandMapping.keySet();
    }

    private static void sleepQuietly(long period){
        try {
            Thread.sleep(period);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static List<String> blocking_getBandsByGenre(String genre){
        sleepQuietly(1000);
        return genreToBandMapping.get(genre);
    }

    static public Map<String, List<String>> getGenreToBandMapping() {
        return genreToBandMapping;
    }
}
