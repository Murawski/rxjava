package pl.promity.demo.swapi;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Planet {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("rotation_period")
    @Expose
    private String rotationPeriod;
    @SerializedName("orbital_period")
    @Expose
    private String orbitalPeriod;
    @SerializedName("diameter")
    @Expose
    private String diameter;
    @SerializedName("climate")
    @Expose
    private String climate;
    @SerializedName("gravity")
    @Expose
    private String gravity;
    @SerializedName("terrain")
    @Expose
    private String terrain;
    @SerializedName("surface_water")
    @Expose
    private String surfaceWater;
    @SerializedName("population")
    @Expose
    private String population;
    @SerializedName("residents")
    @Expose
    private List<Integer> residents = new ArrayList<Integer>();
    @SerializedName("films")
    @Expose
    private List<Integer> films = new ArrayList<Integer>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Planet() {
    }

    /**
     *
     * @param id
     * @param name
     * @param rotationPeriod
     * @param orbitalPeriod
     * @param diameter
     * @param climate
     * @param gravity
     * @param terrain
     * @param surfaceWater
     * @param population
     * @param residents
     * @param films
     */
    public Planet(long id, String name, String rotationPeriod, String orbitalPeriod, String diameter, String climate, String gravity, String terrain, String surfaceWater, String population, List<Integer> residents, List<Integer> films) {
        super();
        this.id = id;
        this.name = name;
        this.rotationPeriod = rotationPeriod;
        this.orbitalPeriod = orbitalPeriod;
        this.diameter = diameter;
        this.climate = climate;
        this.gravity = gravity;
        this.terrain = terrain;
        this.surfaceWater = surfaceWater;
        this.population = population;
        this.residents = residents;
        this.films = films;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRotationPeriod() {
        return rotationPeriod;
    }

    public void setRotationPeriod(String rotationPeriod) {
        this.rotationPeriod = rotationPeriod;
    }

    public String getOrbitalPeriod() {
        return orbitalPeriod;
    }

    public void setOrbitalPeriod(String orbitalPeriod) {
        this.orbitalPeriod = orbitalPeriod;
    }

    public String getDiameter() {
        return diameter;
    }

    public void setDiameter(String diameter) {
        this.diameter = diameter;
    }

    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public String getGravity() {
        return gravity;
    }

    public void setGravity(String gravity) {
        this.gravity = gravity;
    }

    public String getTerrain() {
        return terrain;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public String getSurfaceWater() {
        return surfaceWater;
    }

    public void setSurfaceWater(String surfaceWater) {
        this.surfaceWater = surfaceWater;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public List<Integer> getResidents() {
        return residents;
    }

    public void setResidents(List<Integer> residents) {
        this.residents = residents;
    }

    public List<Integer> getFilms() {
        return films;
    }

    public void setFilms(List<Integer> films) {
        this.films = films;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("rotationPeriod", rotationPeriod)
                .append("orbitalPeriod", orbitalPeriod)
                .append("diameter", diameter)
                .append("climate", climate)
                .append("gravity", gravity)
                .append("terrain", terrain)
                .append("surfaceWater", surfaceWater)
                .append("population", population)
                .append("residents", residents)
                .append("films", films)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Planet planet = (Planet) o;

        return new EqualsBuilder()
                .append(getId(), planet.getId())
                .append(getName(), planet.getName())
                .append(getRotationPeriod(), planet.getRotationPeriod())
                .append(getOrbitalPeriod(), planet.getOrbitalPeriod())
                .append(getDiameter(), planet.getDiameter())
                .append(getClimate(), planet.getClimate())
                .append(getGravity(), planet.getGravity())
                .append(getTerrain(), planet.getTerrain())
                .append(getSurfaceWater(), planet.getSurfaceWater())
                .append(getPopulation(), planet.getPopulation())
                .append(getResidents(), planet.getResidents())
                .append(getFilms(), planet.getFilms())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(getName())
                .append(getRotationPeriod())
                .append(getOrbitalPeriod())
                .append(getDiameter())
                .append(getClimate())
                .append(getGravity())
                .append(getTerrain())
                .append(getSurfaceWater())
                .append(getPopulation())
                .append(getResidents())
                .append(getFilms())
                .toHashCode();
    }

    public long getId() {
        return id;
    }

}