package pl.promity.demo.swapi;

import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import pl.promity.demo.swapi.Person;
import pl.promity.demo.swapi.PersonService;

import static org.junit.Assert.*;

public class PersonServiceTest {

    private PersonService objectUnderTest;

    @Before
    public void setUp() throws Exception {
        this.objectUnderTest = new PersonService();
    }

    @Test
    public void shouldCreateOrderServiceCorrectly() {
        assertNotNull(this.objectUnderTest);
    }

    @Test
    public void shouldReturnAllPersons() throws Exception {
        //given
        TestObserver<Person> testSubscriber = new TestObserver<>();

        //when
        objectUnderTest.findAll().subscribe(testSubscriber);

        //then
        testSubscriber.assertValueCount(83);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();

    }
}