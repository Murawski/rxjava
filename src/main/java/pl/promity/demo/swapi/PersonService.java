package pl.promity.demo.swapi;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.stream.JsonReader;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonService {

    private final Map<Long, Person> cache;

    public PersonService() throws IOException, URISyntaxException {
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,
                (JsonDeserializer<LocalDate>) (json, typeOfT, context) -> LocalDate.parse(json.getAsString())).create();
        try(Stream<Path> filmStream = Files.list(Paths.get(Resources.getResource("people").toURI()))){
            List<Path> films = filmStream.collect(Collectors.toList());
            cache = Observable.fromIterable(films).map(file -> new JsonReader(new FileReader(file.toFile())))
                    .map( reader -> gson.fromJson(reader, Person.class))
                    .cast(Person.class)
                    .toMap(Person::getId)
                    .blockingGet();
        }
    }

    public Observable<Person> findAll(){
        return Observable.fromIterable(cache.values());
    }

    public Maybe<Person> findById(long id) {
        return Maybe.fromCallable(() -> cache.get(id));
    }


}
