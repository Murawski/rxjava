package pl.promity.demo.swapi;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Person {

    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("mass")
    @Expose
    private String mass;
    @SerializedName("hair_color")
    @Expose
    private String hairColor;
    @SerializedName("skin_color")
    @Expose
    private String skinColor;
    @SerializedName("eye_color")
    @Expose
    private String eyeColor;
    @SerializedName("birth_year")
    @Expose
    private String birthYear;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("homeworld")
    @Expose
    private Integer homeworld;
    @SerializedName("films")
    @Expose
    private List<Integer> films = new ArrayList<Integer>();
    @SerializedName("species")
    @Expose
    private List<Integer> species = new ArrayList<Integer>();
    @SerializedName("vehicles")
    @Expose
    private List<Integer> vehicles = new ArrayList<Integer>();
    @SerializedName("starships")
    @Expose
    private List<Integer> starships = new ArrayList<Integer>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Person() {
    }

    /**
     *
     * @param id
     * @param name
     * @param height
     * @param mass
     * @param hairColor
     * @param skinColor
     * @param eyeColor
     * @param birthYear
     * @param gender
     * @param homeworld
     * @param films
     * @param species
     * @param vehicles
     * @param starships
     */
    public Person(long id, String name, String height, String mass, String hairColor, String skinColor, String eyeColor, String birthYear, String gender, Integer homeworld, List<Integer> films, List<Integer> species, List<Integer> vehicles, List<Integer> starships) {
        super();
        this.id = id;
        this.name = name;
        this.height = height;
        this.mass = mass;
        this.hairColor = hairColor;
        this.skinColor = skinColor;
        this.eyeColor = eyeColor;
        this.birthYear = birthYear;
        this.gender = gender;
        this.homeworld = homeworld;
        this.films = films;
        this.species = species;
        this.vehicles = vehicles;
        this.starships = starships;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getSkinColor() {
        return skinColor;
    }

    public void setSkinColor(String skinColor) {
        this.skinColor = skinColor;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getHomeworld() {
        return homeworld;
    }

    public void setHomeworld(Integer homeworld) {
        this.homeworld = homeworld;
    }

    public List<Integer> getFilms() {
        return films;
    }

    public void setFilms(List<Integer> films) {
        this.films = films;
    }

    public List<Integer> getSpecies() {
        return species;
    }

    public void setSpecies(List<Integer> species) {
        this.species = species;
    }

    public List<Integer> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<Integer> vehicles) {
        this.vehicles = vehicles;
    }

    public List<Integer> getStarships() {
        return starships;
    }

    public void setStarships(List<Integer> starships) {
        this.starships = starships;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("height", height)
                .append("mass", mass)
                .append("hairColor", hairColor)
                .append("skinColor", skinColor)
                .append("eyeColor", eyeColor)
                .append("birthYear", birthYear)
                .append("gender", gender)
                .append("homeworld", homeworld)
                .append("films", films)
                .append("species", species)
                .append("vehicles", vehicles)
                .append("starships", starships)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return new EqualsBuilder()
                .append(getId(), person.getId())
                .append(getName(), person.getName())
                .append(getHeight(), person.getHeight())
                .append(getMass(), person.getMass())
                .append(getHairColor(), person.getHairColor())
                .append(getSkinColor(), person.getSkinColor())
                .append(getEyeColor(), person.getEyeColor())
                .append(getBirthYear(), person.getBirthYear())
                .append(getGender(), person.getGender())
                .append(getHomeworld(), person.getHomeworld())
                .append(getFilms(), person.getFilms())
                .append(getSpecies(), person.getSpecies())
                .append(getVehicles(), person.getVehicles())
                .append(getStarships(), person.getStarships())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(getName())
                .append(getHeight())
                .append(getMass())
                .append(getHairColor())
                .append(getSkinColor())
                .append(getEyeColor())
                .append(getBirthYear())
                .append(getGender())
                .append(getHomeworld())
                .append(getFilms())
                .append(getSpecies())
                .append(getVehicles())
                .append(getStarships())
                .toHashCode();
    }

    public long getId() {
        return id;
    }
}