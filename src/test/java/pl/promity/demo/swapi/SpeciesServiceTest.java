package pl.promity.demo.swapi;

import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import pl.promity.demo.swapi.Species;
import pl.promity.demo.swapi.SpeciesService;

import static org.junit.Assert.*;

/**
 * Created by Maciek on 2018-09-19.
 */
public class SpeciesServiceTest {

    private SpeciesService objectUnderTest;

    @Before
    public void setUp() throws Exception {
        this.objectUnderTest = new SpeciesService();
    }

    @Test
    public void shouldCreateOrderServiceCorrectly() {
        assertNotNull(this.objectUnderTest);
    }

    @Test
    public void shouldReturnAllSpecies() throws Exception {
        //given
        TestObserver<Species> testSubscriber = new TestObserver<>();

        //when
        objectUnderTest.findAll().subscribe(testSubscriber);

        //then
        testSubscriber.assertValueCount(35);
        testSubscriber.assertNoErrors();
        testSubscriber.assertComplete();

    }



}