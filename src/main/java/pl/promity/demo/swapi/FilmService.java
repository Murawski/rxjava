package pl.promity.demo.swapi;

import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.stream.JsonReader;
import io.reactivex.Observable;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilmService {

    private final List<Film> filmCache;

    public FilmService() throws IOException, URISyntaxException {
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class,
                (JsonDeserializer<LocalDate>) (json, typeOfT, context) -> LocalDate.parse(json.getAsString())).create();
        try(Stream<Path> filmStream = Files.list(Paths.get(Resources.getResource("films").toURI()))){
            List<Path> films = filmStream.collect(Collectors.toList());
            filmCache = Observable.fromIterable(films).map(file -> new JsonReader(new FileReader(file.toFile())))
                    .map( reader -> gson.fromJson(reader, Film.class))
                    .cast(Film.class)
                    .toList().blockingGet();
        }
    }

    public Observable<Film> findAll(){
        return Observable.fromIterable(filmCache);
    }

}
